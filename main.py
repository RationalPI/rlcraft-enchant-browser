import python_nbt.nbt as nbt
import pygame
import pygame_gui

def main():

    def readMarkersDatas(path="C:/Users/1salm/curseforge/minecraft/Instances/RLCraft v2.9 Dev Build 7/saves/W/data/aaMarkers_0.dat"):
        ret=[]
        file = nbt.read_from_nbt_file("C:/Users/1salm/curseforge/minecraft/Instances/RLCraft v2.9 Dev Build 7/saves/W/data/aaMarkers_0.dat")
        for world in file['data']['dimMap']:
            for marker in world['markers']:
                if(marker['markerType'].value=='antiqueatlas:book'):
                    ret.append({
                        'label':marker['label'].value,
                        'pos':(marker['x'].value,marker['y'].value)
                    })
       
        return ret

    class Rect():
        def __init__(self,markers,bounding=(800,600)):
            self.bounding=bounding
            self.minX=0
            self.minY=0
            self.maxX=0
            self.maxY=0
            for marker in markers:
                p=marker['pos']
                X=p[0]
                Y=p[1]
                if(self.minX>X):
                    self.minX=X
                if(self.maxX<X):
                    self.maxX=X
                if(self.minY>Y):
                    self.minY=Y
                if(self.maxY<Y):
                    self.maxY=Y
        def size(self):
            return (self.maxX-self.minX,self.maxY-self.minY)
        def scale(self):
            size=self.size()
            scaleX=self.bounding[0]/size[0]
            scaleY=self.bounding[1]/size[1]
            return min(scaleX,scaleY)
        def offset(self):
            return (self.minX,self.minY)

    BlueSkyColor = 0, 200, 255
    wSize=(1500, 1000)

    markersDatas=readMarkersDatas()
    rect=Rect(markersDatas,wSize)
    scale=rect.scale()
    offset=rect.offset()


    pygame.init()
    pygame.display.set_caption('RlCraft enchant browser')
    clock = pygame.time.Clock()
    manager = pygame_gui.UIManager(wSize)

    windowSurface = pygame.display.set_mode(wSize)
    bkgr = pygame.Surface(windowSurface.get_size())
    bkgr.fill(BlueSkyColor)

    font = pygame.font.SysFont(None, 20)
    mousePosRender = font.render('', True, (0,0,0))

    markers=[]
    class Marker():
        def __init__(self,markerData):
            w=300
            x=wSize[0]-w
            h=20
            self.data=markerData
            self.pos=((markerData['pos'][0]-offset[0])*scale, (markerData['pos'][1]-offset[1])*scale)
            self.ui_button=pygame_gui.elements.UIButton(
                relative_rect=pygame.Rect((x, 0+len(markers)*h), (w, h)),
                text=markerData['label'],
                manager=manager
            )
            self.selected=False

            self.markerSurface=pygame.Surface((2,2))
            self.markerSurface.fill((0,0,0))
            self.markerSurfaceSelected=pygame.Surface((4,4))
            self.markerSurfaceSelected.fill((255,0,0))
        def handleEvent(self,event):
            if event.type == pygame.USEREVENT:
                if event.user_type == pygame_gui.UI_BUTTON_PRESSED:
                    if event.ui_element == self.ui_button:
                        self.selected=True
                        return
                    self.selected=False
        def blitOn(self,windowSurface):
            if(self.selected):
                pygame.draw.line(windowSurface, (0,0,0), (self.pos[0],-10000), (self.pos[0],+10000))
                pygame.draw.line(windowSurface, (0,0,0), (-10000,self.pos[1]), (+10000,self.pos[1]))
            else:
                windowSurface.blit(self.markerSurface, self.pos)
    for markerData in markersDatas:
        markers.append(Marker(markerData))

    clock = pygame.time.Clock()
    loop=True
    while loop:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                loop = False
            if event.type == pygame.MOUSEMOTION:
                mousePosRender = font.render('{:.0f} {:.0f}'.format(event.pos[0]/scale+offset[0],event.pos[1]/scale+offset[1]), True, (0,0,0))
            manager.process_events(event)
            for marker in markers:
                marker.handleEvent(event)

        windowSurface.blit(bkgr, (0, 0))
        windowSurface.blit(mousePosRender, (0, 0))

        for marker in markers:
            marker.blitOn(windowSurface)
        
        manager.update(clock.tick(60)/1000.0)
        manager.draw_ui(windowSurface)

        pygame.display.update()
        clock.tick(10)

if __name__ == '__main__':
    main()
